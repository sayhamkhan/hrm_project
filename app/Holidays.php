<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
   
    protected $fillable=['holiday_name','from_date','to_date','holiday_description'];

   

}
