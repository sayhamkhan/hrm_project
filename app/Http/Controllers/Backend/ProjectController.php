<?php

namespace App\Http\Controllers\Backend;

use App\Employee;
use http\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Projects;
use App\Clients;
use http\Message;
use Illuminate\Support\Facades\Auth;
use PDF;

class ProjectController extends Controller
{
    public function client_list()
    {
        $clients = Clients::all();

        return view('Projects.client_setup', compact('clients'));

    }

    public function client_create(Request $request)
    {

        $photo = $request->file('image');
        $file_name = uniqid("photo_", true) . str_random(10) . '.' . $photo->getClientOriginalExtension();
        if ($photo->isValid()) {
            $photo->storeAs('images', $file_name);
        }
        Clients::create([
            'client_name' => $request->input('client_name'),
            'image' => $file_name,
            'address' => $request->input('address'),
            'organization' => $request->input('organization'),
            'phone_num' => $request->input('phone_num'),
            'image' => $file_name,

        ]);

        return redirect()->back();
    }

    public function view_client_profile($id)
    {
        $client_info = Clients::where('id', $id)->first();
        return view('Projects.client_profile_view', compact('client_info'));

    }


    public function project_list()
    {
        $id=Auth::user()->id;
        $emplId=Employee::select('id')->Where('user_id',$id)->first();

        $projects = Projects::with('client')->whereIn('employee_id', [$id])->get();
        //dd($projects);
        $project_name = Clients::all();
        $clientName = Clients::all();
        $employeeName = Employee::all();
        return view('Projects.project_list', compact('projects', 'project_name', 'clientName', 'employeeName'));
    }

    public function project_create(Request $request)
    {



        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'tech_spe' => 'required',
            'client_name' => 'required',
            'employee_name' => 'required'


        ]);

//        dd(json_encode($request->input('employee_name')));
        Projects::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'tec_spe' => $request->input('tech_spe'),
            'client_id' => $request->input('client_name'),
            'status' => $request->input('status'),
            'employee_id' => json_encode($request->input('employee_name')),


        ]);
        session()->flash('message', 'Project Created Successfully!!');
        return redirect()->back();

    }

    public function project_update($id)
    {


        $project_info = Projects::where('id', $id)->first();
        return view('Projects.update_project', compact('project_info'));


    }


    public function project_updateprocess(Request $request, $id)
    {


        Projects::where('id', $id)->update([
            'name' => $request->input('project_name'),
            'description' => $request->input('project_description'),
            'tec_spe' => $request->input('technical_specification'),
            'status' => $request->input('status')

        ]);

        session()->flash('message', 'Project Update Successfully!!');
        return redirect()->route('project.list');
    }

    public function client_update($id)
    {
        $client_info=Clients::where('id', $id)->first();
        return view('Projects.client_edit', compact('client_info'));

    }

    public function client_updateprocess( Request $request, $id)
    {
       Clients:: where('id', $id)->update([
           'client_name'=>$request->input('client_name'),
           'address'=>$request->input('address'),
           'organization'=>$request->input('organization'),
           'phone_num'=>$request->input('phone')

       ]);
        session()->flash('message', 'Client Update Successfully!!');
        return redirect()->route('client.list');
        //client.list

    }

}
