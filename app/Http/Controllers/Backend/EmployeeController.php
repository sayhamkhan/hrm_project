<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;
use App\Designation;
use App\Employee;
use App\User;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function list()
    {
        if (auth()->user()->id == 1) {
            // $all_list=Employee::all();
            $employee = DB::table('employees as e')
                ->select('e.*', 'dp.dept_name', 'ds.designation_name')
                ->leftjoin('designations as ds', 'ds.id', 'e.designation_id')
                ->leftjoin('departments as dp', 'dp.id', 'e.department_id')
                ->get();

        } else {
            // $all_list=Employee::where('user_id',auth()->user()->id)->get();
            $employee = DB::table('employees as e')
                ->select('e.*', 'dp.dept_name', 'ds.designation_name')
                //->leftjoin('table name as ds','column name','column name')
                ->leftjoin('designations as ds', 'ds.id', 'e.designation_id')
                ->leftjoin('departments as dp', 'dp.id', 'e.department_id')
                ->where('user_id', auth()->user()->id)->get();

        }

// dd($employee);
        $departments = Department::all();
        $designations = Designation::all();


        return view('emp_management.employee_list', compact('departments', 'designations', 'employee'));

    }

    public function dept()
    {

        $all_dept = Department::all();
        return view('emp_management.department', compact('all_dept'));
    }

    public function desig()
    {

        $all_desig = Designation::all();
        return view('emp_management.designation', compact('all_desig'));
    }

    public function create(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'employee_name' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'email' => 'required|email',
        ]);

        $photo = $request->file('photo');
        $file_name = uniqid("photo_", true) . str_random(10) . '.' . $photo->getClientOriginalExtension();
        if ($photo->isValid()) {
            $photo->storeAs('images', $file_name);
        }
        $user = User::create([
            'email' => $request->input('email'),

            'password' => bcrypt('12345')
        ]);

        Employee::create([
            'user_id' => $user->id,
            'employee_name' => $request->input('employee_name'),
            'email' => $request->input('email'),
            'phone_num' => $request->input('phone_num'),
            'department_id' => $request->input('department_id'),
            'designation_id' => $request->input('designation_id'),
            'photo' => $file_name,
        ]);
        session()->flash('message', 'Employee Created Successfuly');

        return redirect()->back();

    }

    public function update($id)
    {
        $emp_info = Employee::where('id', $id)->first();

        $departments = Department::all();
        $designations = Designation::all();
// dd($emp_info);
        return view('emp_management.update_employee', compact('emp_info', 'departments', 'designations'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'employee_name' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'email' => 'required|email',
        ]);

        $photo = $request->file('photo');

        if (!empty($photo)) {
            // dd($request->all());

            if ($photo->isValid()) {
                $file_name = uniqid("photo_", true) . str_random(10) . '.' . $photo->getClientOriginalExtension();
                $photo->storeAs('images', $file_name);
            }
            Employee::where('id', $id)->update([
                'employee_name' => $request->input('employee_name'),
                'photo' => $file_name,
                'email' => $request->input('email'),
                'phone_num' => $request->input('phone_num'),
                'department_id' => $request->input('department_id'),
                'designation_id' => $request->input('designation_id')
            ]);
        } else {

            Employee::where('id', $id)->update([
                'employee_name' => $request->input('employee_name'),
                'email' => $request->input('email'),
                'phone_num' => $request->input('phone_num'),
                'department_id' => $request->input('department_id'),
                'designation_id' => $request->input('designation_id')
            ]);
        }


        if (auth()->user()->id == 1) {
            // $all_list=Employee::all();
            $employee = DB::table('employees as e')
                ->select('e.*', 'dp.dept_name', 'ds.designation_name')
                ->leftjoin('designations as ds', 'ds.id', 'e.designation_id')
                ->leftjoin('departments as dp', 'dp.id', 'e.department_id')
                ->get();

        } else {
            // $all_list=Employee::where('user_id',auth()->user()->id)->get();
            $employee = DB::table('employees as e')
                ->select('e.*', 'dp.dept_name', 'ds.designation_name')
                //->leftjoin('table name as ds','column name','column name')
                ->leftjoin('designations as ds', 'ds.id', 'e.designation_id')
                ->leftjoin('departments as dp', 'dp.id', 'e.department_id')
                ->where('user_id', auth()->user()->id)->get();

        }

// dd($employee);
        $departments = Department::all();
        $designations = Designation::all();

        session()->flash('message', 'Employee Updated');
        return view('emp_management.employee_list', compact('employee', 'departments', 'designations'));
    }

    public function view($id)
    {

        $emp_info = Employee::where('user_id', $id)->with('department')->with('designation')->first();
        $departments = Department::all();
        $designations = Designation::all();
// dd($emp_info->department->dept_name);
        return view('emp_management.view_emp_profile', compact('emp_info', 'departments', 'designations'));
    }

}
