<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  Auth;
use App\Employee;
use App\Leave;
use App\Holidays;
use Carbon\Carbon;
use Session;


class LeaveController extends Controller
{
  public function leave_requests()
  {

    $employees=Employee::all();
    $leave=Leave::where('user_id',auth()->user()->id)->get();
      // dd($leave);
    return view('leave_management.leave_requests', compact('employees','leave'));
  }

  public function create(Request $request)
  {  

// dd($request->all());
    $today=date('Y-m-d');

    $dt1 =date($request->from_date);
    $dt2 = date($request->to_date);

    $overlapping = Leave::where('user_id',auth()->user()->id)
    ->where(function ($query) use($dt1,$dt2) {
      $query->whereBetween('from_date', [$dt1, $dt2])
      ->orWhereBetween('to_date', [$dt1, $dt2]);
    })->first();

    if(  $dt1<$dt2 and $dt1>$today )
    {
      if(empty($overlapping))
      {
       Leave::create([
        'user_id'=>auth()->user()->id,
        'employee_name'=>$request->input('employee_name'), 
        'from_date'=>$request->input('from_date'),
        'to_date'=>$request->input('to_date'),
        'leave_reason'=>$request->input('leave_reason'),
        'emergency_contact'=>$request->input('emergency_contact'),
      ]);
       Session::flash('message', 'Leave Created!!'); 
       return redirect()->back();
     }
     else
     {
      Session::flash('message', 'You have leave on selected date.'); 
      return redirect()->back();
    }


  } 
  else
  {
   Session::flash('message', 'From date should be grater then today!'); 
   return redirect()->back();
 }



}


public function approve()
{
 $leave=Leave::with('employees')->get();
    // dd($leave->employees);
 return view('leave_management.leave_approval', compact('leave'));


}
public function approveProcess($id,$type)
{

  Leave::where('id',$id)->update([
    'status'=>$type, 

  ]);
  return redirect()->back();


}

}

