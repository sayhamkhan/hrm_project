<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  Auth;
use App\Notice;
use App\Holidays;
use Carbon\Carbon;

class BackendController extends Controller
{


    public  function index()
    {
        if(auth()->user()->id==1)
        {
         return  redirect('admin/dashboard');
     }else
     {

        $notices=Notice::whereDate('created_at','>=',Carbon::today())->get();  
        $holidays= Holidays::whereDate('from_date','>',Carbon::today())->get();
        // dd($holidays);     
        return view('index', compact('notices','holidays'));

    }


}

public  function  login(){

    if (!Auth::check())
    {

      return view('login');
  }

  else
  {

      return  redirect('index');
  }
}


public  function  loginProcess(Request $request)
{
    $request->validate([
        'email'  => 'required|max:255|email',
        'password'  => 'required',
    ]);


    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials)) {
            // Authentication passed...

        return redirect()->intended('index');
    }

    else
    {

     return redirect()->back();
    }

}



public  function  logout(){



    Auth::logout();


    return view('login');
}



}
