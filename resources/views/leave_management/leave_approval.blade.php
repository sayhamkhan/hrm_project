 @extends('master')
 @section('navbar')

    @include('partials.navbar')
@stop

@section('content')

<div class="container-fluid" style="padding-top: 8px;">

   
      <table class="table table-bordered table-condensed" style="margin-top: 5px;">

            <thead>
            <tr>
               
                <th>Employee Name</th>
                <th>From date</th>
                <th>To date</th>
                <th>Total Days</th>
                <th>Leave Reason</th>
                <th>Emergency contact</th>
                <th>Status</th>
                <th>Action</th>
                
            </tr>
            </thead>

            <tbody>
              @foreach($leave as $data)
                <tr>

                    <td>{{$data->employees->employee_name}}</td>
                    <td>{{$data->from_date}}</td>
                    <td>{{$data->to_date}}</td>
                    <td>{{(floor((strtotime($data->to_date)-strtotime($data->from_date))/3600/24))}}</td>
                    <td>{{$data->leave_reason}}</td>
                    <td>{{$data->emergency_contact}}</td>
                    <td>{{$data->status}}</td>
                    <td>
                    	<a class="btn btn-primary form-control" href="{{route('leave.approve',[$data->id,'Approved'])}}">Approve</a>
                    	<a class="btn btn-danger form-control" href="{{route('leave.approve',[$data->id,'Reject'])}}">Reject</a>
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>





</div>


</div>

@endsection

