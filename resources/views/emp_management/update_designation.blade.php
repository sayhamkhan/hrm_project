@extends('master')


@section('navbar')

@include('partials.navbar')
@stop



@section('content')

<style type="text/css">
 .col_text_center{
  margin: auto;
  float: none;
 }
</style>
<div class="row">

    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="col-md-6 col_text_center">
    <div class="p-3 w-100">
    	 <div class="mb-3 text-center">
            <p class="text-uppercase font-w700 font-size-sm text-muted">Update Designation</p>
        </div>
      <!-- Header -->
      <form action="{{route('designation.update',$designation_info->id)}}" method="POST" role="form">

@method('put')
@csrf()
      <div class="form-group">
        <label for="exampleInputName">Designation  Name</label>

        <input name="designation_name" type="name" class="form-control" id="exampleInputName" placeholder="Enter Name" value="{{$designation_info->designation_name}}">
      </div>


  <button  type="submit" class="btn btn-primary form-control">Submit</button>
</form>
</div>
</div>
</div>

@stop