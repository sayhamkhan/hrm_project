

@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop




@section('content')
<style type="text/css">
 .col_text_center{
  margin: auto;
  float: none;
 }
</style>
<div class="row">



<div class="col-md-6 col_text_center">
    <div class="p-3 w-100">
        <!-- Header -->
        <div class="mb-3 text-center">
            
            <p class="text-uppercase font-w700 font-size-sm text-muted">Update Department Information</p>
        </div>
      
        <form action="{{route('department.update',$dept_info->id)}}" method="POST" role="form">
         @method('put')
         @csrf

        <div class="form-group">
            <label for="exampleInputName">Department Name</label>
            <input name="dept_name" type="name" class="form-control" id="exampleInputName" placeholder="Enter Name" value="{{$dept_info->dept_name}}">
          </div>
          
  
          <div class="form-group" >
            <label for="exampleInput">Department Description </label>
            <input name="dept_des" value="{{$dept_info->dept_des}}" type="description" class="form-control" id="exampleInputDescription" aria-describedby="descriptionHelp" placeholder="Enter Description">
          </div>


          <button  type="submit" class="btn btn-primary form-control">Submit</button>
        </form>

      </div>

    </div>
     
    </div>
  </div>



@stop