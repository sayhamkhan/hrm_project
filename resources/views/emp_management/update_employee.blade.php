@extends('master')


@section('navbar')

    @include('partials.navbar')
@stop


@section('content')

<div class="row">

    @if(Session::has('message'))
        <p class="alert alert-success">{{ Session::get('message') }}</p>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif




    <div class="hero-static offset-md-2 col-md-6 d-flex align-items-bg-white">
    <div class="p-3 w-100">
        <!-- Header -->
        <div class="mb-3 text-center">
            <p class="text-uppercase font-w700 font-size-sm text-muted">Update Employee Information</p>
        </div>
        
 <form action="{{route('employee.update',$emp_info->id)}}" method="POST" role="form" enctype="multipart/form-data">

@method('put')
@csrf()

        <div class="form-group">
            <label for="exampleInputName">Employee Name</label>
            <input name="employee_name" type="name" class="form-control" id="exampleInputName" placeholder="Enter Name" value="{{$emp_info->employee_name}}">
          </div>

          <div class="form-group">
            <label for="exampleInputPhoto">Employee Photo</label>
            <input name="photo" type="file" class="form-control" id="exampleInputPhoto" placeholder="Upload Photo" value="">
          </div>
          

          <div class="form-group">
            <label for="exampleInputEmail1">Email </label>
            <input name="email" value="{{$emp_info->email}}" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          </div>


          <div class="form-group">
            <label for="exampleInputNumber">Phone Number</label>
            <input name="phone_num" type="number" value="{{$emp_info->phone_num}}" class="form-control" id="exampleInputNumber" placeholder="Enter phone number">
          </div>

          
          <div class="form-group">
               <label for="dept">Select Department</label>
             <select name="department_id" class="select form-control">
            @foreach($departments as $dept)
              <option  value="{{$dept->id}}" @if($emp_info->department_id==$dept->id) selected @endif>{{$dept->dept_name}}</option>
              @endforeach
            </select>
  
          </div>
 

          <div class="form-group">
               <label for="desig">Select Designation</label>
           <select name="designation_id" class="select form-control">
              @foreach($designations as $desig)
             <option  value="{{$desig->id}}" @if($emp_info->designation_id==$desig->id) selected @endif>{{$desig->designation_name}}</option>
              @endforeach
            </select>
          </div>

          <button  type="submit" class="btn btn-primary form-control">Submit</button>
        </form>
      </div>
     
    </div>
  </div>


@stop