<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('index');
});*/




Route::get('/login', 'Backend\BackendController@login')->name('login');

Route::post('/login', 'Backend\BackendController@loginProcess')->name('loginProcess');



Route::group(['middleware' =>'auth'], function (){
Route::get('/', 'Backend\BackendController@index');
Route::get('/index', 'Backend\BackendController@index')->name('home');


Route::get('/logout', 'Backend\BackendController@logout')->name('logout');


Route::get('/logout', 'Backend\BackendController@logout')->name('logout');

Route::get('/admin/dashboard', 'Backend\DashboardController@view')->name('admin.dashboard');
//Route::get('/admin/dashboard/emp_count', 'Backend\DashboardController@total_employee')->name('emp.count');






Route::get('/department', 'Backend\EmployeeController@dept')->name('department');
Route::post('/dept/create', 'Backend\DepartmentController@create')->name('dept.create');
Route::get('/dept/update/{id}', 'Backend\DepartmentController@update')->name('update_department_page');
Route::put('/department/edit/{id}', 'Backend\DepartmentController@edit')->name('department.update');


Route::get('/designation', 'Backend\DesignationController@list')->name('designation');
Route::post('/designation/create', 'Backend\DesignationController@create')->name('designation.create');
Route::get('/designation/update/{id}','Backend\DesignationController@update')->name('update_designation_page');
Route::put('/designation/edit/{id}', 'Backend\DesignationController@edit')->name('designation.update');


Route::get('/employee', 'Backend\EmployeeController@list')->name('employee.list');
Route::post('/employee/create', 'Backend\EmployeeController@create')->name('employee.create');
Route::get('/employee/update/{id}', 'Backend\EmployeeController@update')->name('update_employee_page');
Route::put('/employee/edit/{id}', 'Backend\EmployeeController@edit')->name('employee.update');
Route::get('/employee/{id}/profile', 'Backend\EmployeeController@view')->name('view_profile');

Route::get('/project', 'Backend\ProjectController@project_list')->name('project.list');
Route::post('/project/create', 'Backend\ProjectController@project_create')->name('project.create');
Route::get('/project/update/{id}', 'Backend\ProjectController@project_update')->name('project.update');
Route::put('/project/edit/{id}', 'Backend\ProjectController@project_updateprocess')->name('project.updateprocess');
Route::get('/client_setup', 'Backend\ProjectController@client_list')->name('client.list');
Route::post('/client_setup/create', 'Backend\ProjectController@client_create')->name('client.create');
Route::get('/client/{id}/profile', 'Backend\ProjectController@view_client_profile')->name('view_client_profile');
Route::get('/client/update/{id}', 'Backend\ProjectController@client_update')->name('client.update');
Route::put('/client/edit/{id}', 'Backend\ProjectController@client_updateprocess')->name('client.updateprocess');






Route::get('/leave/requests', 'Backend\LeaveController@leave_requests')->name('requests');
Route::post('/leave/requests/create', 'Backend\LeaveController@create')->name('request.create');
Route::get('/leave/requests/approve', 'Backend\LeaveController@approve')->name('leave_approval');
Route::get('/leave/requests/approveProcess/{id}/{type}', 'Backend\LeaveController@approveProcess')->name('leave.approve');



Route::get('/view/holidays', 'Backend\NoticeController@holidays')->name('setup_holidays');
Route::post('/holidays/create', 'Backend\NoticeController@holidays_create')->name('holiday.create');
Route::get('/view/notices', 'Backend\NoticeController@notices')->name('view_notices');
Route::post('/notice/create', 'Backend\NoticeController@notice_create')->name('notice.create');
Route::get('/notice/update/{id}', 'Backend\NoticeController@update')->name('update_notice');
Route::put('/notice/edit/{id}', 'Backend\NoticeController@updateprocess')->name('notice.updateprocess');

Route::get('/workreport', 'Backend\WorkReportController@workreport')->name('workreport.list');
Route::post('/workreport/create', 'Backend\WorkReportController@workreport_create')->name('workreport.create');
Route::get('/workreport/update/{id}', 'Backend\WorkReportController@workreport_update')->name('workreport_update');
Route::put('/workreport/edit/{id}', 'Backend\WorkReportController@updateprocess')->name('workreport.updateprocess');





Route::get('/emp_attendance', 'Backend\AttendanceController@show')->name('employee.attendance');
Route::get('/emp_attendance/in', 'Backend\AttendanceController@in')->name('attendance.in');
Route::get('/emp_attendance/out', 'Backend\AttendanceController@out')->name('attendance.out');
Route::get('/emp_attendance/report', 'Backend\AttendanceController@report')->name('attendance.report');
Route::post('/emp_attendance/report', 'Backend\AttendanceController@showReport')->name('report.generate');
Route::get('/attendance_report/pdf','Backend\AttendanceController@export_pdf')->name('report_pdf');





});